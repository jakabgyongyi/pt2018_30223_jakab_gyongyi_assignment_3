package com.tp.hwk.bll;

import com.tp.hwk.model.Address;
import com.tp.hwk.dao.AddressDAO;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressBLL{
    private AddressDAO addressDAO;

    @Autowired
    public AddressBLL(){
        addressDAO = new AddressDAO();
    }

    public boolean addressInsert(Address address){
        return addressDAO.insert(address);
    }

    public boolean addressUpdate(Address address){
        return addressDAO.update(address);
    }

    public boolean addressDelete(Address address){
        return addressDAO.delete(address);
    }

    public Address addressFindByID(int ID){
        return addressDAO.findByID(ID);
    }

    public List<Address> addressFindAllList(){
        return addressDAO.findAllList();
    }

    public ObservableList<Address> addressFindAllObservableList(){
        return addressDAO.findAllObservableList();
    }

    public Address addressFindByCustomerID(int customerID){
        return addressDAO.findByClientID(customerID);
    }

    public boolean cityANDstreetValidation(String city){
        if(city.isEmpty())
            return false;
        String start = city.substring(0,1);
        if(!start.matches("[A-Z]+"))
            return false;
        if(!city.matches("[A-Za-z]+"))
            return false;
        return true;
    }

    public boolean numberValidation(String number){
        if(number.isEmpty())
            return false;
        try{
            int result = Integer.parseInt(number);
            if(result <= 0)
                return false;
        }catch(NumberFormatException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
