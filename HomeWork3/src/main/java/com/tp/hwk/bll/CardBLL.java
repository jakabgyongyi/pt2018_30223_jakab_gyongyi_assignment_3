package com.tp.hwk.bll;

import com.tp.hwk.model.Card;
import com.tp.hwk.dao.CardDAO;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardBLL {
    private CardDAO cardDAO;

    @Autowired
    public CardBLL(){
        cardDAO = new CardDAO();
    }

    public boolean cardInsert(Card card){
        return cardDAO.insert(card);
    }

    public boolean cardUpdate(Card card){
        return cardDAO.update(card);
    }

    public boolean cardDelete(Card card){
        return cardDAO.delete(card);
    }

    public Card cardFindByID(int ID){
        return cardDAO.findByID(ID);
    }

    public List<Card> cardFindAllList(){
        return cardDAO.findAllList();
    }

    public ObservableList<Card> cardFindAllObservableList(){
        return cardDAO.findAllObservableList();
    }

    public Card findByClientID(int clientID){
        return cardDAO.findByClientID(clientID);
    }

    public boolean brandValidation(String brand){
        if(brand.isEmpty())
            return false;
        String start = brand.substring(0,1);
        if(!start.matches("[A-Z]+"))
            return false;
        if(!brand.matches("[A-Za-z]+"))
            return false;
        return true;
    }

    public boolean accountNumberValidation(String accountNumber){
        if(!accountNumber.isEmpty() && accountNumber.matches("[0-9]+"))
            return true;
        return true;
    }

    public boolean cardSecurityNumberValidation(String cardSecurityNumber){
        if(cardSecurityNumber.isEmpty())
            return false;
        try{
            int result = Integer.parseInt(cardSecurityNumber);
            if(result <= 0)
                return false;
        }catch(NumberFormatException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean cardHolderNameValidation(String cardHolderName){
        if(cardHolderName.isEmpty())
            return false;
        String[] splitserult = cardHolderName.split(" ");
        for (String aSplitserult : splitserult) {
            if (!aSplitserult.matches("[A-Za-z.]"))
                return false;
        }
        return true;
    }

}
