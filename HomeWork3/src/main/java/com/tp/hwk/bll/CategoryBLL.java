package com.tp.hwk.bll;

import com.tp.hwk.dao.CategoryDAO;
import com.tp.hwk.model.Category;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryBLL {
    private CategoryDAO categoryDAO;

    @Autowired
    public CategoryBLL(){
        categoryDAO = new CategoryDAO();
    }

    public boolean categoryInsert(Category category){
        return categoryDAO.insert(category);
    }

    public boolean categoryUpdate(Category category){
        return categoryDAO.update(category);
    }

    public boolean categoryDelete(Category category){
        return categoryDAO.delete(category);
    }

    public Category categoryFindByID(int ID){
        return categoryDAO.findByID(ID);
    }

    public List<Category> categoryFindAllList(){
        return categoryDAO.findAllList();
    }

    public ObservableList<Category> categoryFindAllObservableList(){
        return categoryDAO.findAllObservableList();
    }

    public boolean nameValidation(String name){
        String start = name.substring(0,1);
        if(!start.matches("[A-Z]+"))
            return false;
        if(name.matches("[A-Za-z]"))
            return false;
        return true;
    }
}
