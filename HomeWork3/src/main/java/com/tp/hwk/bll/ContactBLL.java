package com.tp.hwk.bll;

import com.tp.hwk.model.Contact;
import com.tp.hwk.dao.ContactDAO;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactBLL {
    private ContactDAO contactDAO;

    @Autowired
    public ContactBLL(){
        contactDAO = new ContactDAO();
    }

    public boolean contactInsert(Contact contact){
        return contactDAO.insert(contact);
    }

    public boolean contactUpdate(Contact contact){
        return contactDAO.update(contact);
    }

    public boolean contactDelete(Contact contact){
        return contactDAO.delete(contact);
    }

    public Contact contactFindByID(int ID){
        return contactDAO.findByID(ID);
    }

    public Contact contactFindByClientID(int clientID){
        return contactDAO.findByClientID(clientID);
    }

    public List<Contact> contactFindAllList(){
        return contactDAO.findAllList();
    }

    public ObservableList<Contact> contactFindAllObservableList(){
        return contactDAO.findAllObservableList();
    }

    public boolean emailValidation(String email){
        if(email.isEmpty())
            return false;
        if(!email.matches("[A-Za-z0-9!#$%&*+/=?^_{|}~]+") || !email.contains(".com"))
            return false;
        return true;
    }

    public boolean telnumberValidation(String telnumber){
        if(telnumber.isEmpty())
            return false;
        if(!telnumber.matches("[0-9]+"))
            return false;
        return true;
    }

}
