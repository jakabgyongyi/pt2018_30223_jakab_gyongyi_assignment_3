package com.tp.hwk.bll;

import com.tp.hwk.dao.CustomerDAO;
import com.tp.hwk.model.Customer;
import com.tp.hwk.model.specialModels.CustomerOrderView;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerBLL {
    private CustomerDAO customerDAO;

    @Autowired
    public CustomerBLL(){
        customerDAO = new CustomerDAO();
    }

    public Customer createCustomer(String lastName, String firstName, String userName, String password){
        Customer customer = new Customer(0, lastName, firstName, userName, password);
        return customer;
    }

    public boolean lastNameValidation(String Name){
        //Last or Fist name needs to start with uppercase letter and only have characters, or - and space
        String[] splitresult = Name.split("-");
        for (String aSplitresult : splitresult) {
            if (!aSplitresult.matches("[A-Za-z]+"))
                return false;
        }
        return true;
    }

    public boolean firstNameValidation(String Name){
        //Last or Fist name needs to start with uppercase letter and only have characters, or - and space
        String[] splitresult = Name.split(" ");
        for (String aSplitresult : splitresult) {
            if (!aSplitresult.matches("[A-Za-z]+"))
                return false;
        }
        return true;
    }

    public boolean userNameValidation(String userName){
        if(userName.matches("[A-Za-z0-9]+"))
            return true;
        return false;
    }

    public boolean passwordValidation(String password){
        if(password.matches("[A-Za-z0-9~@#$&+_?!]+"))
            return true;
        return false;
    }

    public boolean customerInsert(Customer customer){
        return customerDAO.insert(customer);
    }

    public boolean customerUpdate(Customer customer){
        return customerDAO.update(customer);
    }

    public boolean customerDelete(Customer customer){
        return customerDAO.delete(customer);
    }

    public List<Customer> customerFindAllInList(){
        return customerDAO.findAllList();
    }

    public ObservableList<Customer> customerFindAllInObservableList(){
        return customerDAO.findAllObservableList();
    }

    public ObservableList<CustomerOrderView> findAllOrders(int ID){
        return customerDAO.findAllOrders(ID);
    }

    public TableView<Customer> customerCreateTable(){
        return customerDAO.createTable();
    }

    public Customer customerLogin(String userName, String password){
        return customerDAO.customerLogin(userName, password);
    }

    public Customer customerFindByUserName(String userName){
        return customerDAO.findByUserName(userName);
    }
}
