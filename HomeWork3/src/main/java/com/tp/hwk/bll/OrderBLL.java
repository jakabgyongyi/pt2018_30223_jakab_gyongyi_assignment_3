package com.tp.hwk.bll;

import com.tp.hwk.dao.CustomerDAO;
import com.tp.hwk.dao.OrderDAO;
import com.tp.hwk.dao.ProductDAO;
import com.tp.hwk.model.*;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderBLL {
    private OrderDAO orderDAO;
    private ProductDAO productDAO;
    private CustomerDAO customerDAO;

    @Autowired
    public OrderBLL(){
        orderDAO = new OrderDAO();
        productDAO = new ProductDAO();
        customerDAO = new CustomerDAO();
    }

    public boolean orderInsert(ProOrder order){
        return orderDAO.insert(order);
    }

    public boolean orderUpdate(ProOrder order){
        return orderDAO.update(order);
    }

    public boolean orderDelete(ProOrder order){
        return orderDAO.delete(order);
    }

    public ProOrder orderFindByID(int ID){
        return orderDAO.findByID(ID);
    }

    public List<ProOrder> orderFindByClientID(int clientID){
        return orderDAO.findByClientID(clientID);
    }

    public List<ProOrder> orderFindByProductID(int productID){
        return orderDAO.findByProductID(productID);
    }

    public List<ProOrder> orderFindAllList(){
        return orderDAO.findAllList();
    }

    public ObservableList<ProOrder> orderFindAllObservableList(){
        return orderDAO.findAllObservableList();
    }

    public boolean clientIDValidation(String clientID){
        int clientIDInt;
        try{
            clientIDInt = Integer.parseInt(clientID);
            Customer customer = customerDAO.findByID(clientIDInt);
            if(customer == null){
                return false;
            }
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public boolean cproductIDValidation(String productID){
        int productIDInt;
        try{
            productIDInt = Integer.parseInt(productID);
            Product product = productDAO.findByID(productIDInt);
            if(product == null){
                return false;
            }
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public boolean quantityValidation(String quantity){
        double quantityDouble;
        try{
            quantityDouble = Double.parseDouble(quantity);
            if(quantityDouble <= 0.0){
                return false;
            }
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public boolean totalPriceValidation(String totalPrice){
        double totalPriceDouble;
        try{
            totalPriceDouble = Double.parseDouble(totalPrice);
            if(totalPriceDouble <= 0.0){
                return false;
            }
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }
}
