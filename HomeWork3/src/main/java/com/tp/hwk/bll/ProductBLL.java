package com.tp.hwk.bll;

import com.tp.hwk.model.Product;
import com.tp.hwk.dao.ProductDAO;
import com.tp.hwk.model.Provider;
import com.tp.hwk.model.Stock;
import com.tp.hwk.model.specialModels.ProductInfoView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductBLL {
    private ProductDAO productDAO;

    @Autowired
    public ProductBLL(){
        productDAO = new ProductDAO();
    }

    public Product createProduct(int subCat, String name, String specification, String description){
        Product product = new Product(0,subCat, name, specification, description);
        return product;
    }

    public boolean productInsert(Product product){
        return productDAO.insert(product);
    }

    public boolean productUpdate(Product product){
        return productDAO.update(product);
    }

    public boolean productdelete(Product product){
        return productDAO.delete(product);
    }

    public Product productFindByName(String name){
        return productDAO.findByName(name);
    }

    public ObservableList<String> findAllBySubCategoryID(int subCategoryID){
        ObservableList<Product> searchFor = productDAO.findAllBySubCatofyId(subCategoryID);
        ObservableList<String> searchForString = FXCollections.observableArrayList();
        for(Product itr: searchFor){
            searchForString.add(itr.getName());
        }
        return searchForString;
    }

    public ObservableList<ProductInfoView> productStock(Product product){
        List<Stock> poductStock = (new StockBLL()).stockFindAllByProductID(product.getID());
        ObservableList<ProductInfoView> items = FXCollections.observableArrayList();
        for(Stock itr: poductStock){
            Provider provider = (new ProviderBLL()).providerFindByID(itr.getProviderID());
            ProductInfoView piv = new ProductInfoView();
            piv.setProviderName(provider.getProviderName());
            piv.setPrice(itr.getPrice());
            piv.setDiscount(itr.getDiscount());
            piv.setFinalPrice(itr.getPrice() - itr.getPrice()*itr.getDiscount());
            piv.setGuarantee(itr.getGuarantee());
            if(itr.getQuantity() > 0)
                piv.setStock("On stock");
            else
                piv.setStock("Out of stock");
            items.add(piv);
        }
        return items;
    }

    public List<Product> productsFindAllList(){
        return productDAO.findAllList();
    }

    public ObservableList<Product> productsFindAllObservableList(){
        return productDAO.findAllObservableList();
    }
}
