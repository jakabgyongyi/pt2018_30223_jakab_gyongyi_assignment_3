package com.tp.hwk.bll;

import com.tp.hwk.model.Provider;
import com.tp.hwk.dao.ProviderDAO;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderBLL {
    private ProviderDAO providerDAO;

    @Autowired
    public ProviderBLL(){
       providerDAO = new ProviderDAO();
    }


    public boolean providerInsert(Provider provider){
        return providerDAO.insert(provider);
    }

    public boolean providerUpdate(Provider provider){
        return providerDAO.update(provider);
    }

    public boolean provoderDelete(Provider provider){
        return providerDAO.delete(provider);
    }

    public Provider providerFindByID(int ID){
        return providerDAO.findByID(ID);
    }

    public Provider providerFindByName(String name){
        return providerDAO.findByName(name);
    }

    public List<Provider> providerFindAllList(){
        return providerDAO.findAllList();
    }

    public ObservableList<Provider> providerFindAllObservableList(){
        return providerDAO.findAllObservableList();
    }
}
