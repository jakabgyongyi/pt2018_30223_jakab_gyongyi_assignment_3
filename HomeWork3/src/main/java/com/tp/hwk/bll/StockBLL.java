package com.tp.hwk.bll;

import com.tp.hwk.model.*;
import com.tp.hwk.dao.*;
import com.tp.hwk.model.specialModels.ProductInfoView;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockBLL {
    private StockDAO stockDAO;
    private ProviderDAO providerDAO;
    private ProductDAO productDAO;

    @Autowired
    public StockBLL(){
        stockDAO = new StockDAO();
        productDAO = new ProductDAO();
        providerDAO = new ProviderDAO();
    }

    public Stock createStock(String productID,String providerID, String price, String discount, String guarantee, String quantity){
        return new Stock(0,Integer.parseInt(productID), Integer.parseInt(providerID), Double.parseDouble(price), Double.parseDouble(discount), Integer.parseInt(guarantee), Integer.parseInt(quantity));
    }

    public boolean stockInsert(Stock stock){
        return stockDAO.insert(stock);
    }

    public boolean stockUpdate(Stock stock){
        return stockDAO.update(stock);
    }

    public boolean stockDelete(Stock stock){
        return stockDAO.delete(stock);
    }

    public Stock stockFindByID(int ID){
        return stockDAO.findByID(ID);
    }

    public List<Stock> stockFindAll(){
        return stockDAO.findAllList();
    }

    public ObservableList<Stock> stockFindAllObservableList(){
        return stockDAO.findAllObservableList();
    }

    public List<Stock> stockFindAllByProductID(int productID){
        return stockDAO.findByProductID(productID);
    }


    public List<Stock> stockFindAllByProviderID(int providerID){
        return stockDAO.findByProviderID(providerID);
    }

    public Stock stockFindByPtoviderIDAndProductID(int productID, int providerID){
        return stockDAO.findByProviderIDAndProductID(providerID, productID);
    }

    public boolean providerIDValidation(String providerID){
        int providerId = 0;
        try{
            providerId = Integer.parseInt(providerID);
            Provider provider = providerDAO.findByID(providerId);
            if(provider == null)
                return false;
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public boolean productIDValidation(String productID){
        int productId = 0;
        try{
            productId = Integer.parseInt(productID);
            Product product = productDAO.findByID(productId);
            if(product == null)
                return false;
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public boolean priceValidation(String price){
        double priceDouble = 0.0;
        try{
            priceDouble = Double.parseDouble(price);
            if(priceDouble <= 0.0)
                return false;
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public boolean discountValidation(String discount){
        double discountDouble = 0.0;
        try{
            discountDouble = Double.parseDouble(discount);
            if(discountDouble < 0.0 || discountDouble > 1.0)
                return false;
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public boolean guaranteeValidation(String guarantee){
        int guaranteeInt = 0;
        try{
            guaranteeInt = Integer.parseInt(guarantee);
            if(guaranteeInt < 6 || guaranteeInt > 60)
                return false;
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public boolean quantityValidation(String quantity){
        int quantityInt = 0;
        try{
            quantityInt = Integer.parseInt(quantity);
            if(quantityInt < 0 || quantityInt > 100)
                return false;
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }
}
