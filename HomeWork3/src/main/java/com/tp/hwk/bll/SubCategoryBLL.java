package com.tp.hwk.bll;

import com.tp.hwk.model.SubCategory;
import com.tp.hwk.dao.SubCategoryDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubCategoryBLL {
    @Autowired
    private SubCategoryDAO subCategoryDAO;
    @Autowired
    public SubCategoryBLL(){
        subCategoryDAO = new SubCategoryDAO();
    }

    public boolean nameValidation(String name){
        if(name.matches("[A-Za-z]+"))
            return true;
        return false;
    }

    public boolean subcategoryInsert(SubCategory subCategory){
        return subCategoryDAO.insert(subCategory);
    }

    public boolean subcategoryUpdate(SubCategory subCategory){
        return subCategoryDAO.update(subCategory);
    }

    public boolean subcategoryDelete(SubCategory subCategory){
        return subCategoryDAO.delete(subCategory);
    }

    public ObservableList<String> subcategoryFindAllByCategoryID(int categoryID){
        ObservableList<SubCategory> subcat = subCategoryDAO.findByCategoryID(categoryID);
        ObservableList<String> subcatString = FXCollections.observableArrayList();
        for(SubCategory itr: subcat){
            subcatString.add(itr.getName());
        }
        return subcatString;
    }

    public SubCategory findByName(String name){
        return subCategoryDAO.findByName(name);
    }

    public List<SubCategory> subcategoryFindAllList(){
        return subCategoryDAO.findAllList();
    }

    public ObservableList<SubCategory> subCategoryFindAllObservableList(){
        return subCategoryDAO.findAllObservableList();
    }
}
