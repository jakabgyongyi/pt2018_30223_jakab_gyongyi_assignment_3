package com.tp.hwk.controllers;

import com.tp.hwk.bll.*;
import com.tp.hwk.config.StageManager;
import com.tp.hwk.model.*;
import com.tp.hwk.view.AlertBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.sql.Timestamp;
import java.util.ResourceBundle;

@Controller
public class AdminAccount implements Initializable {
    private final CustomerBLL customerBLL = new CustomerBLL();
    private final ProductBLL productBLL = new ProductBLL();
    private final SubCategoryBLL subCategoryBLL = new SubCategoryBLL();
    private final ProviderBLL providerBLL = new ProviderBLL();
    private final StockBLL stockBLL = new StockBLL();
    private final OrderBLL orderBLL = new OrderBLL();
    private final ContactBLL contactBLL = new ContactBLL();
    private final AddressBLL addressBLL = new AddressBLL();
    @Lazy
    @Autowired
    private StageManager stageManager;

    @FXML
    private TableView<Customer> customerTable;
    @FXML
    private TextField lastName;
    @FXML
    private TextField firstName;
    @FXML
    private TextField userName;
    @FXML
    private TextField password;
    @FXML
    private TableColumn<Customer, Integer> customerIDCol;
    @FXML
    private TableColumn<Customer, String> customerLastNameCol;
    @FXML
    private TableColumn<Customer, String> customerFirstNameCol;
    @FXML
    private TableColumn<Customer, String> customerUserNameCol;
    @FXML
    private TableColumn<Customer, String> customerPasswordCol;
    @FXML
    private TableView<Product> productTable;
    @FXML
    private TableColumn<Product, Integer> productIDCol;
    @FXML
    private TableColumn<Product, Integer> productSubCatCol;
    @FXML
    private TableColumn<Product, String> productNameCol;
    @FXML
    private TableColumn<Product, String> productDescriptionCol;
    @FXML
    private TableColumn<Product, String> productSpecificationCol;
    @FXML
    private TextField productName;
    @FXML
    private TextArea productDescription;
    @FXML
    private TextArea productSpecification;
    @FXML
    private ChoiceBox<String> productSubCat;
    @FXML
    private TableView<Stock> stockTable;
    @FXML
    private TableColumn<Stock, Integer> stockStockIDCol;
    @FXML
    private TableColumn<Stock, Integer> stockProductCol;
    @FXML
    private TableColumn<Stock, Integer> stockProviderCol;
    @FXML
    private TableColumn<Stock, Double> stockPriceCol;
    @FXML
    private TableColumn<Stock, Double> stockDiscountCol;
    @FXML
    private TableColumn<Stock, Integer> stockquaranteeCol;
    @FXML
    private TableColumn<Stock, Integer> stockQuantityCol;
    @FXML
    private ChoiceBox<String> stockProductName;
    @FXML
    private ChoiceBox<String> stockProviderName;
    @FXML
    private TextField stockDiscount;
    @FXML
    private TextField stockGuarantee;
    @FXML
    private TextField stockQuantity;
    @FXML
    private TextField stockPrice;
    @FXML
    private TableView<Provider> providerTable;
    @FXML
    private TableColumn<Provider, Integer> providerProviderCol;
    @FXML
    private TableColumn<Provider, String> providerNameCol;
    @FXML
    private TableColumn<Provider, String> providerAddressCol;
    @FXML
    private TableColumn<Provider, String> providerContactCol;
    @FXML
    private TableColumn<Provider, String> providerBankaccountCol;
    @FXML
    private TextField providerProviderName;
    @FXML
    private TextField providerAddress;
    @FXML
    private TextField providerContact;
    @FXML
    private TextField providerBankAccount;
    @FXML
    private TableView<ProOrder> orderTable;
    @FXML
    private TableColumn<ProOrder, Integer> orderOrderCol;
    @FXML
    private TableColumn<ProOrder, Integer> orderClientCol;
    @FXML
    private TableColumn<ProOrder, Integer> orderStockCol;
    @FXML
    private TableColumn<ProOrder, Timestamp> orderTimeCol;
    @FXML
    private TableColumn<ProOrder, Integer> orderQuantityCol;
    @FXML
    private TableColumn<ProOrder, Double> orderTotalPriceCol;
    @FXML
    private TableColumn<ProOrder, String> orderShippingCol;
    @FXML
    private ChoiceBox<String> orderClientusername;
    @FXML
    private ChoiceBox<String> orderProductName;
    @FXML
    private ChoiceBox<String> orderProviderName;
    @FXML
    private ChoiceBox<String> orderShipping;
    @FXML
    private TextField orderQuantity;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        initializeProductTable();
        initializeCustomerTable();
        initializeStockTable();
        initializeProviderTable();
        initializeOrderTable();
    }

    @FXML
    private void handleCustomerInsert(){
        if(lastName.getText().isEmpty() || firstName.getText().isEmpty() || userName.getText().isEmpty() || password.getText().isEmpty())
            return;
        if(!customerBLL.lastNameValidation(lastName.getText())){
            lastName.setStyle("-fx-text-inner-color: red;");
            return;
        }
        if(!customerBLL.firstNameValidation(firstName.getText())){
            firstName.setStyle("-fx-text-inner-color: red;");
            return;
        }
        if(!customerBLL.userNameValidation(userName.getText())){
            userName.setStyle("-fx-text-inner-color: red;");
            return;
        }
        if(!customerBLL.passwordValidation(password.getText())){
            password.setStyle("-fx-text-inner-color: red;");
            return;
        }
        Customer newcustomer = customerBLL.createCustomer(lastName.getText(), firstName.getText(), userName.getText(), password.getText());
        if(newcustomer != null){
            System.out.println("Created a valid Customer");
            if(customerBLL.customerInsert(newcustomer)){
                System.out.println("Customer was successfully inserted in database");
                customerTable.setItems(customerBLL.customerFindAllInObservableList());
                customerTable.refresh();
                resetCustomer();
            }
        }
    }

    @FXML
    private void handleCustomerUpdate(){
        if(!lastName.getText().isEmpty()) {
            if (!customerBLL.lastNameValidation(lastName.getText())) {
                lastName.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }
        if(!firstName.getText().isEmpty()) {
            if (!customerBLL.firstNameValidation(firstName.getText())) {
                firstName.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }
        if(!userName.getText().isEmpty()) {
            if (!customerBLL.userNameValidation(userName.getText())) {
                userName.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }
        if(!password.getText().isEmpty()) {
            if (!customerBLL.passwordValidation(password.getText())) {
                password.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }
        Customer newcustomer = customerBLL.createCustomer(lastName.getText(), firstName.getText(), userName.getText(), password.getText());
        if(newcustomer != null){
            System.out.println("Created a valid Customer");
            ObservableList<Customer> selectedItems = customerTable.getSelectionModel().getSelectedItems();
            if(!selectedItems.isEmpty()){
                Customer updateableCustomer = selectedItems.get(0);
                if(!newcustomer.getLastName().isEmpty())
                    updateableCustomer.setLastName(newcustomer.getLastName());
                if(!newcustomer.getFirstName().isEmpty())
                    updateableCustomer.setFirstName(newcustomer.getFirstName());
                if(!newcustomer.getUserName().isEmpty())
                    updateableCustomer.setUserName(newcustomer.getUserName());
                if(!newcustomer.getPassword().isEmpty())
                    updateableCustomer.setPassword(newcustomer.getPassword());
                if(customerBLL.customerUpdate(updateableCustomer)){
                    System.out.println("Updated a customer succesfully");
                    customerTable.setItems(customerBLL.customerFindAllInObservableList());
                    customerTable.refresh();
                    resetCustomer();
                }
            }
        }
    }

    @FXML
    private void handleCustomerDelete(){
        ObservableList<Customer> selectedItems = customerTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            Customer deletableCustomer = selectedItems.get(0);
            if(customerBLL.customerDelete(deletableCustomer)){
                System.out.println("Deleted a customer succesfully");
                customerTable.setItems(customerBLL.customerFindAllInObservableList());
                customerTable.refresh();
            }
        }
    }

    @FXML
    private void handleProductInsert(){
        if(productName.getText().isEmpty())
            return;
        if(productDescription.getText().isEmpty())
            return;
        if(productSpecification.getText().isEmpty())
            return;
        if(productSubCat.getSelectionModel().getSelectedItem().isEmpty())
            return;
        SubCategory searchfor = subCategoryBLL.findByName(productSubCat.getValue());
        if(searchfor != null){
            Product newproduct = productBLL.createProduct(searchfor.getID(), productName.getText(),productSpecification.getText(), productDescription.getText());
            if(newproduct != null){
                System.out.println("Successfully created a new product");
                if(productBLL.productInsert(newproduct)){
                    System.out.println("Succesfully inserted a product");
                    productTable.setItems(productBLL.productsFindAllObservableList());
                    productTable.refresh();
                    resetProduct();
                }
            }
        }
    }

    @FXML
    private void handleProductUpdate(){
        SubCategory searchfor = null;
        if(!productSubCat.getValue().isEmpty())
            searchfor = subCategoryBLL.findByName(productSubCat.getValue());
        Product newproduct = null;
        if(searchfor != null)
            newproduct = productBLL.createProduct(searchfor.getID(), productName.getText(),productSpecification.getText(), productDescription.getText());
        else
            newproduct = productBLL.createProduct(0, productName.getText(),productSpecification.getText(), productDescription.getText());
        if(newproduct != null) {
            System.out.println("Successfully created a new product");
            ObservableList<Product> selectedItems = productTable.getSelectionModel().getSelectedItems();
            if (!selectedItems.isEmpty()) {
                Product updateableproduct = selectedItems.get(0);
                if (newproduct.getSubCategoryID() != 0)
                    updateableproduct.setSubCategoryID(newproduct.getSubCategoryID());
                if (!newproduct.getDescription().isEmpty())
                    updateableproduct.setDescription(newproduct.getDescription());
                if (!newproduct.getSpecification().isEmpty())
                    updateableproduct.setSpecification(newproduct.getSpecification());
                if (!newproduct.getName().isEmpty())
                    updateableproduct.setName(newproduct.getName());
                if (productBLL.productUpdate(updateableproduct)) {
                    System.out.println("Updated succesfully a product");
                    productTable.setItems(productBLL.productsFindAllObservableList());
                    productTable.refresh();
                    resetProduct();
                }
            }
        }
    }

    @FXML
    private void handleProductDelete(){
        ObservableList<Product> selectedItems = productTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            Product deletableproduct = selectedItems.get(0);
            if(productBLL.productdelete(deletableproduct)){
                System.out.println("Deleted succesfully a product");
                productTable.setItems(productBLL.productsFindAllObservableList());
                productTable.refresh();
                resetProduct();
            }
        }
    }

    @FXML
    private void resetCustomer(){
        lastName.setStyle("-fx-text-inner-color: black;");
        firstName.setStyle("-fx-text-inner-color: black;");
        userName.setStyle("-fx-text-inner-color: black;");
        password.setStyle("-fx-text-inner-color: black;");
        lastName.clear();
        firstName.clear();
        userName.clear();
        password.clear();
    }

    @FXML
    private void resetProduct(){
        productName.clear();
        productDescription.clear();
        productSpecification.clear();
        productSubCat.setValue("SmartPhones");
    }

    @FXML
    private void handleStockInsert(){
        if(!stockDiscount.getText().isEmpty()) {
            if (!stockBLL.discountValidation(stockDiscount.getText())) {
                stockDiscount.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }else return;

        if(!stockGuarantee.getText().isEmpty()) {
            if (!stockBLL.guaranteeValidation(stockGuarantee.getText())) {
                stockGuarantee.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }else return;
        if(!stockQuantity.getText().isEmpty()) {
            if (!stockBLL.quantityValidation(stockQuantity.getText())) {
                stockQuantity.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }else return;
        if(!stockPrice.getText().isEmpty()) {
            if (!stockBLL.priceValidation(stockPrice.getText())) {
                stockPrice.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }else return;
        if(stockProductName.getSelectionModel().getSelectedItem().isEmpty())
            return;
        if(stockProviderName.getSelectionModel().getSelectedItem().isEmpty())
            return;
        Stock stock = new Stock();
        Product product = productBLL.productFindByName(stockProductName.getValue());
        Provider provider = providerBLL.providerFindByName(stockProviderName.getValue());
        stock.setProductID(product.getID());
        stock.setProviderID(provider.getID());
        stock.setPrice(Double.parseDouble(stockPrice.getText()));
        stock.setDiscount(Double.parseDouble(stockDiscount.getText()));
        stock.setGuarantee(Integer.parseInt(stockGuarantee.getText()));
        stock.setQuantity(Integer.parseInt(stockQuantity.getText()));
        if(stockBLL.stockInsert(stock)){
            System.out.println("Stock insert was successful");
            stockTable.setItems(stockBLL.stockFindAllObservableList());
            stockTable.refresh();
            resetStock();
        }
    }

    @FXML
    private void handleStockDelete(){
        ObservableList<Stock> selectedItems = stockTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            Stock stock = selectedItems.get(0);
            if(stockDiscount.getText().isEmpty()) {
                if (!stockBLL.discountValidation(stockDiscount.getText())) {
                    stockDiscount.setStyle("-fx-text-inner-color: red;");
                    return;
                } else {
                    stock.setDiscount(Double.parseDouble(stockDiscount.getText()));
                }
            }
            if(stockGuarantee.getText().isEmpty()) {
                if (!stockBLL.guaranteeValidation(stockGuarantee.getText())) {
                    stockGuarantee.setStyle("-fx-text-inner-color: red;");
                    return;
                } else {
                    stock.setGuarantee(Integer.parseInt(stockGuarantee.getText()));
                }
            }
            if(stockQuantity.getText().isEmpty()) {
                if (!stockBLL.quantityValidation(stockQuantity.getText())) {
                    stockQuantity.setStyle("-fx-text-inner-color: red;");
                    return;
                }
                else{
                    stock.setQuantity(Integer.parseInt(stockQuantity.getText()));
                }
            }
            if(stockPrice.getText().isEmpty()) {
                if (!stockBLL.priceValidation(stockPrice.getText())) {
                    stockPrice.setStyle("-fx-text-inner-color: red;");
                    return;
                }
                else{
                    stock.setPrice(Double.parseDouble(stockPrice.getText()));
                }
            }
            if(stockProductName.getSelectionModel().getSelectedItem().isEmpty())
                return;
            else
                stock.setProductID(productBLL.productFindByName(stockProviderName.getValue()).getID());
            if(stockProviderName.getSelectionModel().getSelectedItem().isEmpty())
                return;
            else
                stock.setProviderID(providerBLL.providerFindByName(stockProviderName.getValue()).getID());
            if(stockBLL.stockUpdate(stock)){
                System.out.println("Update stock was successful");
            }
        }
    }

    @FXML
    private void handleStockUpdate(){
        ObservableList<Stock> selectedItems = stockTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            Stock stock = selectedItems.get(0);
            if(stockBLL.stockDelete(stock)){
                System.out.println("Delete was successful");
                stockTable.setItems(stockBLL.stockFindAllObservableList());
                stockTable.refresh();
            }
        }
    }

    private void resetStock(){
        stockDiscount.setStyle("-fx-text-inner-color: black;");
        stockGuarantee.setStyle("-fx-text-inner-color: black;");
        stockQuantity.setStyle("-fx-text-inner-color: black;");
        stockPrice.setStyle("-fx-text-inner-color: black;");
        stockDiscount.clear();
        stockGuarantee.clear();
        stockQuantity.clear();
        stockPrice.clear();
    }

    @FXML
    private void handleProviderInsert(){
        if(providerProviderName.getText().isEmpty())
            return;
        if(providerAddress.getText().isEmpty())
            return;
        if(providerContact.getText().isEmpty())
            return;
        if(providerBankAccount.getText().isEmpty())
            return;
        Provider provider = new Provider();
        provider.setProviderName(providerProviderName.getText());
        provider.setContact(providerContact.getText());
        provider.setAddress(providerAddress.getText());
        provider.setBankaccount(providerBankAccount.getText());
        if(providerBLL.providerInsert(provider)){
            System.out.println("Insert was succesfull");
        }
        providerTable.setItems(providerBLL.providerFindAllObservableList());
        providerTable.refresh();
    }

    @FXML
    private void handleProviderDelete(){
        ObservableList<Provider> selectedItems = providerTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            Provider provider = selectedItems.get(0);
            if(providerBLL.providerUpdate(provider)){
                System.out.println("Provider delete was succesfull");
            }
        }
    }

    @FXML
    private void handleProviderUpdate(){
        ObservableList<Provider> selectedItems = providerTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            Provider provider = selectedItems.get(0);
            if(!providerProviderName.getText().isEmpty())
                provider.setProviderName(providerProviderName.getText());
            if(!providerContact.getText().isEmpty())
                provider.setContact(providerContact.getText());
            if(!providerAddress.getText().isEmpty())
                provider.setAddress(providerAddress.getText());
            if(!providerBankAccount.getText().isEmpty())
                provider.setBankaccount(providerBankAccount.getText());
            if(providerBLL.providerUpdate(provider)){
                System.out.println("Provider update was succesfull");
            }
        }
    }

    @FXML
    private void handleOrderInsert(){
        if(orderClientusername.getSelectionModel().getSelectedItem().isEmpty())  return;
        if(orderProductName.getSelectionModel().getSelectedItem().isEmpty())  return;
        if(orderProviderName.getSelectionModel().getSelectedItem().isEmpty())   return;
        if(orderShipping.getSelectionModel().getSelectedItem().isEmpty())   return;
        if(!orderQuantity.getText().isEmpty()){
            if(!orderBLL.quantityValidation(orderQuantity.getText())){
                orderQuantity.setStyle("-fx-text-inner-color: red;");
                return;
            }
        }else return;
        ProOrder proOrder = new ProOrder();
        Customer customer = customerBLL.customerFindByUserName(orderClientusername.getValue());
        Contact contact = contactBLL.contactFindByClientID(customer.getID());
        Address address = addressBLL.addressFindByCustomerID(customer.getID());
        proOrder.setClientID(customer.getID());
        Provider provider = providerBLL.providerFindByName(orderProviderName.getValue());
        Product product = productBLL.productFindByName(orderProductName.getValue());
        Stock stock = stockBLL.stockFindByPtoviderIDAndProductID(provider.getID(), product.getID());
        proOrder.setStockID(stock.getID());
        if(stock.getQuantity() >= Integer.parseInt(orderQuantity.getText()))
            proOrder.setQuantity(Integer.parseInt(orderQuantity.getText()));
        else {
            AlertBox.display("Error Frame", "Stock is not sufficient for this order!");
            return;
        }
        proOrder.setTotalPrice(proOrder.getQuantity() * (stock.getPrice() - stock.getPrice()*stock.getDiscount()));
        proOrder.setShipping(orderShipping.getValue());
        if(orderBLL.orderInsert(proOrder)){
            System.out.println("Order insert was successful");
            stock.setQuantity(stock.getQuantity() - proOrder.getQuantity());
            stockBLL.stockUpdate(stock);
            orderTable.setItems(orderBLL.orderFindAllObservableList());
            orderTable.refresh();
            orderQuantity.setStyle("-fx-text-inner-color: black;");
        }
    }

    @FXML
    private void handleOrderDelete(){
        ObservableList<ProOrder> selectedItems = orderTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            ProOrder proOrder = selectedItems.get(0);
            if(orderBLL.orderDelete(proOrder)){
                System.out.println("Order successfully deleted");
                orderTable.setItems(orderBLL.orderFindAllObservableList());
                orderTable.refresh();
            }
        }
    }

    @FXML
    private void handleOrderUpdate(){
        ObservableList<ProOrder> selectedItems = orderTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            ProOrder proOrder = selectedItems.get(0);
            if(!orderClientusername.getSelectionModel().getSelectedItem().isEmpty()){
                proOrder.setClientID(customerBLL.customerFindByUserName(orderClientusername.getValue()).getID());
            }
            if(!orderQuantity.getText().isEmpty()){
                if(!orderBLL.quantityValidation(orderQuantity.getText())){
                    orderQuantity.setStyle("-fx-text-inner-color: red;");
                    return;
                }
            }else return;
            if(!orderProductName.getSelectionModel().getSelectedItem().isEmpty() && !orderProviderName.getSelectionModel().getSelectedItem().isEmpty()){
                Provider provider = providerBLL.providerFindByName(orderProviderName.getValue());
                Product product = productBLL.productFindByName(orderProductName.getValue());
                Stock stock = stockBLL.stockFindByPtoviderIDAndProductID(provider.getID(), product.getID());
                proOrder.setStockID(stock.getID());
                proOrder.setTotalPrice(proOrder.getQuantity() * (stock.getPrice() - stock.getPrice()*stock.getDiscount()));
                if(stock.getQuantity() >= Integer.parseInt(orderQuantity.getText()))
                    proOrder.setQuantity(Integer.parseInt(orderQuantity.getText()));
                else {
                    AlertBox.display("Error Frame", "Stock is not sufficient for this order!");
                    return;
                }
            }
            if(!orderShipping.getSelectionModel().getSelectedItem().isEmpty()){
                proOrder.setShipping(orderShipping.getValue());
            }
            if(orderBLL.orderUpdate(proOrder)){
                System.out.println("Order successfully updated");
                orderTable.setItems(orderBLL.orderFindAllObservableList());
                orderTable.refresh();
                orderQuantity.setStyle("-fx-text-inner-color: black;");
            }
        }
    }

    private void initializeProductTable(){
        productTable.getColumns().removeAll(productIDCol,productSubCatCol,productNameCol,productDescriptionCol,productSpecificationCol);
        productIDCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
        productSubCatCol.setCellValueFactory(new PropertyValueFactory<>("subCategoryID"));
        productNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        productSpecificationCol.setCellValueFactory(new PropertyValueFactory<>("specification"));
        productDescriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        productTable.setItems(productBLL.productsFindAllObservableList());
        productTable.getColumns().addAll(productIDCol,productSubCatCol,productNameCol,productSpecificationCol,productDescriptionCol);
        productTable.refresh();
        ObservableList<SubCategory> subcatlist = subCategoryBLL.subCategoryFindAllObservableList();
        ObservableList<String> subcatstringlist = FXCollections.observableArrayList();
        for(SubCategory itr: subcatlist){
            subcatstringlist.add(itr.getName());
        }
        productSubCat.setItems(subcatstringlist);
        productSubCat.setValue(subcatstringlist.get(0));
    }

    private void initializeCustomerTable(){
        customerTable.getColumns().removeAll(customerIDCol, customerLastNameCol,customerFirstNameCol,customerUserNameCol,customerPasswordCol);
        customerIDCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
        customerLastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        customerFirstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        customerUserNameCol.setCellValueFactory(new PropertyValueFactory<>("userName"));
        customerPasswordCol.setCellValueFactory(new PropertyValueFactory<>("password"));
        customerTable.setItems(customerBLL.customerFindAllInObservableList());
        customerTable.getColumns().addAll(customerIDCol, customerLastNameCol,customerFirstNameCol,customerUserNameCol,customerPasswordCol);
        customerTable.refresh();
    }

    private void initializeStockTable(){
        stockTable.getColumns().removeAll(stockStockIDCol,stockProductCol,stockProviderCol,stockPriceCol,stockDiscountCol,stockquaranteeCol,stockQuantityCol);
        stockStockIDCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
        stockProductCol.setCellValueFactory(new PropertyValueFactory<>("productID"));
        stockProviderCol.setCellValueFactory(new PropertyValueFactory<>("providerID"));
        stockPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        stockDiscountCol.setCellValueFactory(new PropertyValueFactory<>("discount"));
        stockquaranteeCol.setCellValueFactory(new PropertyValueFactory<>("guarantee"));
        stockQuantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        stockTable.getColumns().addAll(stockStockIDCol,stockProductCol,stockProviderCol,stockPriceCol,stockDiscountCol,stockquaranteeCol,stockQuantityCol);
        stockTable.setItems(stockBLL.stockFindAllObservableList());
        stockTable.refresh();

        ObservableList<Product> products = productBLL.productsFindAllObservableList();
        ObservableList<String> productString = FXCollections.observableArrayList();
        for(Product itr: products){
            productString.add(itr.getName());
        }
        stockProductName.setItems(productString);
        if(!productString.isEmpty())
            stockProductName.setValue(productString.get(0));

        ObservableList<Provider> providers = providerBLL.providerFindAllObservableList();
        ObservableList<String> providerString = FXCollections.observableArrayList();
        for(Provider itr: providers){
            providerString.add(itr.getProviderName());
        }
        stockProviderName.setItems(providerString);
        if(!providerString.isEmpty())
            stockProviderName.setValue(providerString.get(0));
    }

    private void initializeProviderTable(){
        providerTable.getColumns().removeAll(providerProviderCol,providerNameCol,providerAddressCol,providerContactCol,providerBankaccountCol);
        providerProviderCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
        providerNameCol.setCellValueFactory(new PropertyValueFactory<>("providerName"));
        providerAddressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        providerContactCol.setCellValueFactory(new PropertyValueFactory<>("contact"));
        providerBankaccountCol.setCellValueFactory(new PropertyValueFactory<>("bankaccount"));
        providerTable.getColumns().addAll(providerProviderCol,providerNameCol,providerAddressCol,providerContactCol,providerBankaccountCol);
        providerTable.setItems(providerBLL.providerFindAllObservableList());
        providerTable.refresh();
    }

    private void initializeOrderTable(){
        orderTable.getColumns().removeAll(orderOrderCol,orderClientCol,orderStockCol,orderTimeCol,orderQuantityCol,orderTotalPriceCol,orderShippingCol);
        orderOrderCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
        orderClientCol.setCellValueFactory(new PropertyValueFactory<>("clientID"));
        orderStockCol.setCellValueFactory(new PropertyValueFactory<>("stockID"));
        orderTimeCol.setCellValueFactory(new PropertyValueFactory<>("dateTime"));
        orderQuantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        orderTotalPriceCol.setCellValueFactory(new PropertyValueFactory<>("totalPrice"));
        orderShippingCol.setCellValueFactory(new PropertyValueFactory<>("shipping"));
        orderTable.getColumns().addAll(orderOrderCol,orderClientCol,orderStockCol,orderTimeCol,orderQuantityCol,orderTotalPriceCol,orderShippingCol);
        orderTable.setItems(orderBLL.orderFindAllObservableList());
        orderTable.refresh();

        ObservableList<Customer> customers = customerBLL.customerFindAllInObservableList();
        ObservableList<Product> products = productBLL.productsFindAllObservableList();
        ObservableList<Provider> providers = providerBLL.providerFindAllObservableList();
        ObservableList<String> content = FXCollections.observableArrayList();
        for(Customer itr:customers){
            content.add(itr.getUserName());
        }
        orderClientusername.setItems(content);
        if(!content.isEmpty())
            orderClientusername.setValue(content.get(0));
        content = FXCollections.observableArrayList();
        for(Product itr: products){
            content.add(itr.getName());
        }
        orderProductName.setItems(content);
        if(!content.isEmpty()){
            orderProductName.setValue(content.get(0));
        }
        content = FXCollections.observableArrayList();
        for(Provider itr: providers){
            content.add(itr.getProviderName());
        }
        orderProviderName.setItems(content);
        if(!content.isEmpty())
            orderProviderName.setValue(content.get(0));
        content = FXCollections.observableArrayList();
        content.add("Free shipping");
        content.add("Pick up by customer");
        orderShipping.setItems(content);
        orderShipping.setValue(content.get(0));
    }
}
