package com.tp.hwk.controllers;

import com.tp.hwk.bll.*;
import com.tp.hwk.config.StageManager;
import com.tp.hwk.model.*;
import com.tp.hwk.model.specialModels.CustomerOrderView;
import com.tp.hwk.view.AlertBox;
import com.tp.hwk.view.FxmlView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.sql.Timestamp;
import java.util.ResourceBundle;

@Controller
public class CustomerAccount implements Initializable{
    private final Customer customer = (new CustomerBLL()).customerFindByUserName("jakabgyi97");
    @Autowired
    private CustomerBLL customerBLL = new CustomerBLL();
    @Autowired
    private AddressBLL addressBLL = new AddressBLL();
    @Autowired
    private ContactBLL contactBLL = new ContactBLL();
    @Autowired
    private CardBLL cardBLL = new CardBLL();
    @Autowired
    private OrderBLL orderBLL = new OrderBLL();
    @FXML
    private Label oldpassword;
    @FXML
    private PasswordField newpassword;
    @FXML
    private PasswordField renewpassword;
    @FXML
    private TextField email;
    @FXML
    private TextField telnumber;
    @FXML
    private ComboBox<String> city;
    @FXML
    private TextField street;
    @FXML
    private TextField number;
    @FXML
    private TextField postcode;
    @FXML
    private CheckBox visacheck;
    @FXML
    private CheckBox maestrocheck;
    @FXML
    private CheckBox mastercardcheck;
    @FXML
    private TextField accountnum;
    @FXML
    private TextField securitynum;
    @FXML
    private TextField cardholdname;
    @FXML
    private TableView<CustomerOrderView> ordertable;
    @FXML
    private TableColumn<CustomerOrderView, String> productNameCol;
    @FXML
    private TableColumn<CustomerOrderView, String> providerNameCol;
    @FXML
    private TableColumn<CustomerOrderView, Timestamp> timeCol;
    @FXML
    private TableColumn<CustomerOrderView, Double> priceCol;
    @FXML
    private TableColumn<CustomerOrderView, Double> discountCol;
    @FXML
    private TableColumn<CustomerOrderView, Double> finalPriceCol;
    @FXML
    private TableColumn<CustomerOrderView, Integer> guaranteeCol;
    @Lazy
    @Autowired
    private StageManager stageManager;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        customerBLL = new CustomerBLL();
        addressBLL = new AddressBLL();
        contactBLL = new ContactBLL();
        cardBLL = new CardBLL();
        orderBLL = new OrderBLL();

        oldpassword.setText(customer.getPassword());
        Contact contact = contactBLL.contactFindByClientID(customer.getID());
        Address address = addressBLL.addressFindByCustomerID(customer.getID());
        Card card = cardBLL.findByClientID(customer.getID());
        email.setText(contact.getEmail());
        telnumber.setText(contact.getTelnumber());
        ObservableList<String> cities = FXCollections.observableArrayList();
        cities.addAll("Cluj-Napoca", "Targu-Mures", "Vulcan", "Satu-Mare", "Bucuresti", "Tudra", "Timisoara", "Brasov");
        city.setItems(cities);
        city.setValue(address.getCity());
        street.setText(address.getStreet());
        number.setText(String.valueOf(address.getNumber()));
        postcode.setText(String.valueOf(address.getPostcode()));
        accountnum.setText(card.getAccountNumber());
        securitynum.setText(String.valueOf(card.getCardSecurityNumber()));
        cardholdname.setText(card.getCardHolderName());
        if(card.getBrand().compareTo("VisaElectron")==0)
            visacheck.setSelected(true);
        if(card.getBrand().compareTo("Maestro")==0)
            maestrocheck.setSelected(true);
        if(card.getBrand().compareTo("MasterCard")==0)
            mastercardcheck.setSelected(true);

        ordertable.getColumns().removeAll(productNameCol,providerNameCol,timeCol,priceCol,discountCol,finalPriceCol,guaranteeCol);
        productNameCol.setCellValueFactory(new PropertyValueFactory<>("productName"));
        providerNameCol.setCellValueFactory(new PropertyValueFactory<>("providerName"));
        timeCol.setCellValueFactory(new PropertyValueFactory<>("time"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        discountCol.setCellValueFactory(new PropertyValueFactory<>("discount"));
        finalPriceCol.setCellValueFactory(new PropertyValueFactory<>("finalPrice"));
        guaranteeCol.setCellValueFactory(new PropertyValueFactory<>("guarantee"));
        ordertable.getColumns().addAll(productNameCol,providerNameCol,timeCol,priceCol,discountCol,finalPriceCol,guaranteeCol);
        ordertable.setItems(customerBLL.findAllOrders(customer.getID()));
        ordertable.refresh();
    }

    @FXML
    private void handlePasswordChange(){
        if(newpassword.getText().isEmpty() && renewpassword.getText().isEmpty()) return;
        if(newpassword.getText().compareTo(renewpassword.getText()) != 0) return;
        if(customer.getPassword().compareTo(newpassword.getText()) == 0) return;
        if(customerBLL.passwordValidation(newpassword.getText())){
            customer.setPassword(newpassword.getText());
            if(customerBLL.customerUpdate(customer)){
                System.out.println("Succesful client Password Update");
                AlertBox.display("Password Update", "You successfully updated your password!");
            }
        }
    }

    @FXML
    private void handleGoBackrequest(){
        stageManager.switchScene(FxmlView.MARKET);
    }

    @FXML
    private void handleContactChange(){
        Contact contact = contactBLL.contactFindByClientID(customer.getID());
        boolean valid = false;
        if(!email.getText().isEmpty()) {
            if (contact.getEmail().compareTo(email.getText()) != 0) {
                if (contactBLL.emailValidation(email.getText())) {
                    contact.setEmail(email.getText());
                    valid = true;
                } else email.setStyle("-fx-text-inner-color: red;");
            }
        }
        if(!telnumber.getText().isEmpty()) {
            if (contact.getTelnumber().compareTo(telnumber.getText()) != 0) {
                if (contactBLL.telnumberValidation(telnumber.getText())) {
                    contact.setTelnumber(telnumber.getText());
                } else {
                    email.setStyle("-fx-text-inner-color: red;");
                    return;
                }
            }
        }
        if(valid){
            if(contactBLL.contactUpdate(contact)){
                System.out.println("Successful contact Update");
                AlertBox.display("Contact Update", "You seccessfully updated your contact information!");
            }
        }

    }

    @FXML
    private void handleAddressChange(){
        Address address = addressBLL.addressFindByCustomerID(customer.getID());
        boolean valid = true;
        if(!city.getSelectionModel().isEmpty()){
            if(address.getCity().compareTo(city.getValue()) != 0){
                if(addressBLL.cityANDstreetValidation(city.getValue())){
                    address.setCity(city.getValue());
                }else {city.setStyle("-fx-text-inner-color: red;");valid = false;}
            }else {city.setStyle("-fx-text-inner-color: red;");valid = false;}
        }
        if(!street.getText().isEmpty()){
            if(address.getStreet().compareTo(street.getText()) != 0){
                if(addressBLL.cityANDstreetValidation(street.getText())){
                    address.setStreet(street.getText());
                }else {street.setStyle("-fx-text-inner-color: red;");valid = false;}
            }else {street.setStyle("-fx-text-inner-color: red;");valid = false;}
        }
        if(!number.getText().isEmpty()){
            if(addressBLL.numberValidation(number.getText())){
                if(address.getNumber() != Integer.parseInt(number.getText())){
                    address.setNumber(Integer.parseInt(number.getText()));
                }else {number.setStyle("-fx-text-inner-color: red;");valid = false;}
            }else {number.setStyle("-fx-text-inner-color: red;");valid = false;}
        }
        if(!postcode.getText().isEmpty()){
            if(addressBLL.numberValidation(postcode.getText())){
                if(address.getPostcode() != Integer.parseInt(postcode.getText())){
                    address.setPostcode(Integer.parseInt(postcode.getText()));
                }else {postcode.setStyle("-fx-text-inner-color: red;");valid = false;}
            }else {postcode.setStyle("-fx-text-inner-color: red;");valid = false;}
        }
        if(valid){
            if(addressBLL.addressUpdate(address)){
                System.out.println("Successful address Update");
                AlertBox.display("Address Update", "You seccessfully updated your address information!");
            }
        }
    }

    @FXML
    private void handleCardChange(){
        Card card = cardBLL.findByClientID(customer.getID());
        String brand = null;
        if(visacheck.isSelected())
            brand = "VisaElectron";
        else if(maestrocheck.isSelected())
            brand = "Maestro";
        else if(mastercardcheck.isSelected())
            brand = "MasterCard";
        if(card.getBrand().compareTo(brand) != 0)
            card.setBrand(brand);
        boolean valid = true;
        if(!accountnum.getText().isEmpty()){
            if(cardBLL.accountNumberValidation(accountnum.getText())){
                if(card.getAccountNumber().compareTo(accountnum.getText()) == 0){
                    card.setAccountNumber(accountnum.getText());
                }else {accountnum.setStyle("-fx-text-inner-color: red;");valid = false;}
            }else {accountnum.setStyle("-fx-text-inner-color: red;");valid = false;}
        }
        if(!securitynum.getText().isEmpty()){
            if(cardBLL.cardSecurityNumberValidation(securitynum.getText())){
                if(card.getCardSecurityNumber() != Integer.parseInt(securitynum.getText())){
                    card.setCardSecurityNumber(Integer.parseInt(securitynum.getText()));
                }else {securitynum.setStyle("-fx-text-inner-color: red;");valid = false;}
            }else {securitynum.setStyle("-fx-text-inner-color: red;");valid = false;}
        }
        if(!cardholdname.getText().isEmpty()){
            if(cardBLL.cardHolderNameValidation(cardholdname.getText())){
                if(card.getAccountNumber().compareTo(cardholdname.getText()) == 0){
                    card.setCardHolderName(cardholdname.getText());
                }else {cardholdname.setStyle("-fx-text-inner-color: red;");valid = false;}
            }else {cardholdname.setStyle("-fx-text-inner-color: red;");valid = false;}
        }else return;
        if(valid){
            if(cardBLL.cardUpdate(card)){
                System.out.println("Successful card Update");
                AlertBox.display("Card Update", "You seccessfully updated your card information!");
            }
        }
    }
}
