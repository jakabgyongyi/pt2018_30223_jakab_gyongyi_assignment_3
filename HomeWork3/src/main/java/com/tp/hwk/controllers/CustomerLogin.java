package com.tp.hwk.controllers;

import com.tp.hwk.bll.*;
import com.tp.hwk.config.StageManager;
import com.tp.hwk.model.*;
import com.tp.hwk.pdfs.PdfCreater;
import com.tp.hwk.view.FxmlView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

@Controller
public class CustomerLogin implements Initializable{
    @Autowired
    private CustomerBLL customerBLL;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @Lazy
    @Autowired
    private StageManager stageManager;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        customerBLL = new CustomerBLL();
    }

    @FXML
    private void handleMouseClick(ActionEvent event){
        if(username.getText().isEmpty() || password.getText().isEmpty())
            return;
        if(!customerBLL.userNameValidation(username.getText()))
            return;
        if(!customerBLL.passwordValidation(password.getText()))
            return;
        Customer customer = customerBLL.customerLogin(username.getText(), password.getText());
        if(customer != null){
            System.out.println("Succesfull login");
            stageManager.switchScene(FxmlView.CLIENTACCOUNT);
        }
        username.clear();
        password.clear();
    }

    @FXML
    private void handleForgotPasswordRequest(ActionEvent event){}

    @FXML
    private void handleGoBackRequest(ActionEvent event){
        stageManager.switchScene(FxmlView.MARKET);
    }
}
