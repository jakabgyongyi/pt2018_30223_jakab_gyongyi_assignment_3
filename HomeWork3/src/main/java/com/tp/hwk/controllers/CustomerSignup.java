package com.tp.hwk.controllers;

import com.tp.hwk.bll.CustomerBLL;
import com.tp.hwk.config.StageManager;
import com.tp.hwk.model.Customer;
import com.tp.hwk.view.AlertBox;
import com.tp.hwk.view.ConfirmBox;
import com.tp.hwk.view.FxmlView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.util.ResourceBundle;

@Controller
public class CustomerSignup implements Initializable{
    @Autowired
    private CustomerBLL customerBLL;
    @Lazy
    @Autowired
    private StageManager stageManager;
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField userName;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField repassword;
    @FXML
    private CheckBox checkTerms;

    @FXML
    private void handleSignUpRequest(ActionEvent event){
        StringBuilder sb = new StringBuilder();
        boolean valid = true;
        if(!firstName.getText().isEmpty()){
            if(!customerBLL.firstNameValidation(firstName.getText())){
                firstName.setStyle("-fx-text-inner-color: red;");
                sb.append("Invalid First name!Please enter a valid First name!\n");
                valid = false;
            }
        }else {
            sb.append("First name field is mandatory!Please enter a valid First name\n");
            valid = false;
        }
        if(!lastName.getText().isEmpty()){
            if(!customerBLL.lastNameValidation(lastName.getText())){
                lastName.setStyle("-fx-text-inner-color: red;");
                sb.append("Invalid First name!Please enter a valid First name!\n");
                valid = false;
            }
        }else {
            sb.append("Last name field is mandatory!Please enter a valid Last name\n");
            valid = false;
        }
        if(!userName.getText().isEmpty()){
            if(!customerBLL.userNameValidation(userName.getText())){
                userName.setStyle("-fx-text-inner-color: red;");
                sb.append("Invalid User name!Please enter a valid User name!\n");
                valid = false;
            }
        }else {
            sb.append("User name field is mandatory!Please enter a valid User name\n");
            valid = false;
        }
        if(!password.getText().isEmpty()){
            if(!customerBLL.passwordValidation(password.getText())){
                password.setStyle("-fx-text-inner-color: red;");
                sb.append("Invalid password!Please enter a valid password!\n");
                valid = false;
            }
        }else  {
            sb.append("Password field is mandatory!Please enter a valid password\n");
            valid = false;
        }
        if(!repassword.getText().isEmpty()){
            if(!customerBLL.passwordValidation(repassword.getText())){
                repassword.setStyle("-fx-text-inner-color: red;");
                sb.append("Invalid password!Please enter a valid password!\n");
                valid = false;
            }
        }else {
            sb.append("Reenter of password field is mandatory!Please enter a valid password\n");
            valid = false;
        }
        if(!checkTerms.isSelected()){
            sb.append("Before official sign up you need to check and agree with our system's Terms and Conditions specifications\n");
            valid = false;
        }
        if(password.getText().compareTo(repassword.getText()) != 0){
            sb.append("Password mismatched! Reenter password!\n");
            valid = false;
        }
        if(valid){
            Customer customer = new Customer();
            customer.setFirstName(firstName.getText());
            customer.setLastName(lastName.getText());
            customer.setUserName(userName.getText());
            customer.setPassword(password.getText());
            if(customerBLL.customerInsert(customer)){
                System.out.println("Client: "+customer+" signup was successful");
                if(ConfirmBox.display("SignUp Confirm", "Do you want to go to the Market?"))
                    stageManager.switchScene(FxmlView.MARKET);
                if(ConfirmBox.display("SignUp Confirm", "Do you want to go to Your Account?"))
                    stageManager.switchScene(FxmlView.CLIENTACCOUNT);
            }
        }else
            AlertBox.display("Error in SignUp", sb.toString());
    }

    @FXML
    private void handleTermsAndConditions(ActionEvent event){
        System.out.println("TO DO: Create terms and conditions window");
    }

    @FXML
    private void handleGoBackRequest(ActionEvent event){
        stageManager.switchScene(FxmlView.MARKET);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        customerBLL = new CustomerBLL();
    }
}
