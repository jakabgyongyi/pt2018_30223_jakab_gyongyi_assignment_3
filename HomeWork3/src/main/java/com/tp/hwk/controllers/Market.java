package com.tp.hwk.controllers;

import com.tp.hwk.bll.*;
import com.tp.hwk.config.StageManager;
import com.tp.hwk.view.FxmlView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import com.tp.hwk.model.*;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.util.ResourceBundle;

@Controller
public class Market implements Initializable{
    private final SubCategoryBLL subCategoryBLL = new SubCategoryBLL();
    private final ProductBLL productBLL = new ProductBLL();
    @Lazy
    @Autowired
    private StageManager stageManager;

    @FXML
    private TextField searchbar;
    @FXML
    private ListView<String> productview;
    @FXML
    private Hyperlink accountlink;
    @FXML
    private ListView<String> laptopCategoryView;
    @FXML
    private ListView<String> electonicscategoryView;
    @FXML
    private ListView<String> componentCategoryView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        laptopCategoryView.setItems(subCategoryBLL.subcategoryFindAllByCategoryID(1));
        electonicscategoryView.setItems(subCategoryBLL.subcategoryFindAllByCategoryID(2));
        componentCategoryView.setItems(subCategoryBLL.subcategoryFindAllByCategoryID(3));
        laptopCategoryView.refresh();
        electonicscategoryView.refresh();
        componentCategoryView.refresh();
    }

    public void handleSearchEvent(ActionEvent event){
        if(searchbar.getText().isEmpty())
            return;
        Product searchFor = productBLL.productFindByName(searchbar.getText());
        if(searchFor != null){
            ObservableList<String> products = FXCollections.observableArrayList();
            products.add(searchFor.getName());
            productview.setItems(products);
            searchbar.clear();
        }
    }

    public void handleProductView(ActionEvent event){
        stageManager.switchScene(FxmlView.PRODUCTVIEW);
    }

    public void handleLoginRequest(ActionEvent event){
       stageManager.switchScene(FxmlView.CLIENTLOGIN);
    }

    public void handleSignUpRequest(ActionEvent event){
        stageManager.switchScene(FxmlView.CLIENTSIGNUP);
    }

    public void handleAccountView(ActionEvent event){
        stageManager.switchScene(FxmlView.CLIENTACCOUNT);
    }

    public void handleFilterRequest(ActionEvent event){
        ObservableList<String> selectedItemsOne = laptopCategoryView.getSelectionModel().getSelectedItems();
        ObservableList<String> selectedItemsTwo = electonicscategoryView.getSelectionModel().getSelectedItems();
        ObservableList<String> selectedItemsThree = componentCategoryView.getSelectionModel().getSelectedItems();

        productview.setItems(null);
        productview.refresh();

        if(selectedItemsOne != null){
            String subCategoryName = selectedItemsOne.get(0);
            filterProduct(subCategoryName);
            return;
        }
        if(selectedItemsTwo != null){
            String subCategoryName = selectedItemsTwo.get(0);
            filterProduct(subCategoryName);
            return;
        }
        if(selectedItemsThree != null){
            String subCategoryName = selectedItemsThree.get(0);
            filterProduct(subCategoryName);
            return;
        }
    }

    private void filterProduct(String SubCategoryName){
        SubCategory subcat = subCategoryBLL.findByName(SubCategoryName);
        if(subcat != null){
            ObservableList<String> products = productBLL.findAllBySubCategoryID(subcat.getID());
            if(!products.isEmpty()){
                productview.setItems(products);
                productview.refresh();
            }
        }
    }
}
