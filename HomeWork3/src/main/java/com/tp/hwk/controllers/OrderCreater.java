package com.tp.hwk.controllers;

import com.tp.hwk.bll.AddressBLL;
import com.tp.hwk.bll.ContactBLL;
import com.tp.hwk.bll.OrderBLL;
import com.tp.hwk.bll.StockBLL;
import com.tp.hwk.config.StageManager;
import com.tp.hwk.model.*;
import com.tp.hwk.pdfs.PdfCreater;
import com.tp.hwk.view.AlertBox;
import com.tp.hwk.view.FxmlView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

@Controller
public class OrderCreater implements Initializable{
    private final Customer customer;
    private final Provider provider;
    private final Product product;
    private final StockBLL stockBLL = new StockBLL();
    private final OrderBLL orderBLL = new OrderBLL();

    @Lazy
    @Autowired
    private StageManager stageManager;

    @FXML
    private Label productName;
    @FXML
    private Label providerName;
    @FXML
    private CheckBox payWithCard;
    @FXML
    private CheckBox payWithCash;
    @FXML
    private CheckBox paymentOrder;
    @FXML
    private Spinner<Integer> quantitySpinner;

    @Lazy
    @Autowired
    public OrderCreater(Customer customer, Provider provider, Product product){
        this.customer = customer;
        this.provider = provider;
        this.product = product;
    }
    @FXML
    private void handleCreateOrderRequest(){
        Stock stock = stockBLL.stockFindByPtoviderIDAndProductID(product.getID(), provider.getID());
        ProOrder proOrder = new ProOrder();
        proOrder.setClientID(customer.getID());
        proOrder.setStockID(stock.getID());
        proOrder.setQuantity(quantitySpinner.getValue());
        proOrder.setTotalPrice(stock.getPrice() - stock.getPrice() * stock.getDiscount());
        String shipping = null;
        if(payWithCard.isSelected())
            shipping = "Free shipping to customer Address";
        else if(payWithCash.isSelected())
            shipping = "Pick up by customer";
        else if(paymentOrder.isSelected())
            shipping = "Pick up by customer";
        proOrder.setShipping(shipping);
        if(orderBLL.orderInsert(proOrder)){
            System.out.println("A successful order was delivered");
            AlertBox.display("Order Manager", "You successfully orderd a product!");
            Contact contact = (new ContactBLL()).contactFindByClientID(customer.getID());
            Address address = (new AddressBLL()).addressFindByCustomerID(customer.getID());
            ArrayList<Object> things = new ArrayList<>();
            things.add(product);
            things.add(provider);
            things.add(customer);
            things.add(address);
            things.add(contact);
            things.add(proOrder);
            PdfCreater.createPDF("Bill.pdf",things);
            stageManager.switchScene(FxmlView.MARKET);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Stock stock = stockBLL.stockFindByPtoviderIDAndProductID(product.getID(), provider.getID());
        productName.setText(product.getName());
        providerName.setText(provider.getProviderName());
        quantitySpinner.setEditable(true);
        quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, stock.getQuantity()));
    }
}
