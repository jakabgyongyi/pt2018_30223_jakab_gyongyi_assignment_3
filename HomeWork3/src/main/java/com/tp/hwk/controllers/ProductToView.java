package com.tp.hwk.controllers;

import com.tp.hwk.bll.*;
import com.tp.hwk.config.StageManager;
import com.tp.hwk.model.specialModels.ProductInfoView;
import com.tp.hwk.model.Product;
import com.tp.hwk.view.FxmlView;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import java.net.URL;
import java.util.ResourceBundle;

@Controller
public class ProductToView implements Initializable{
    private final Product product = new Product();
    @Lazy
    @Autowired
    private StageManager stageManager;

    @FXML
    private Label productName;
    @FXML
    private TableView<ProductInfoView> infoTable;
    @FXML
    private TableColumn<ProductInfoView, String> providerNameCol;
    @FXML
    private TableColumn<ProductInfoView, Double> priceCol;
    @FXML
    private TableColumn<ProductInfoView, Double> discountCol;
    @FXML
    private TableColumn<ProductInfoView, Double> finalPriceCol;
    @FXML
    private TableColumn<ProductInfoView, Integer> guaranteeCol;
    @FXML
    private TableColumn<ProductInfoView, String> stockCol;
    @FXML
    private TextArea infoArea;
    @FXML
    private ImageView productShow;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        infoArea.setText(product.getDescription());
        productName.setText(product.getName());
        infoTable.getColumns().removeAll(providerNameCol,priceCol,discountCol,finalPriceCol,guaranteeCol,stockCol);
        providerNameCol.setCellValueFactory(new PropertyValueFactory<>("providerName"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        discountCol.setCellValueFactory(new PropertyValueFactory<>("discount"));
        finalPriceCol.setCellValueFactory(new PropertyValueFactory<>("finalPrice"));
        guaranteeCol.setCellValueFactory(new PropertyValueFactory<>("guarantee"));
        stockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        infoTable.getColumns().addAll(providerNameCol,priceCol,discountCol,finalPriceCol,guaranteeCol,stockCol);
        infoTable.setItems((new ProductBLL()).productStock(product));
        infoTable.refresh();
    }

    @FXML
    private void handleToggleRequest(ActionEvent event){
        if(infoArea.getText().compareTo(product.getDescription()) == 0)
            infoArea.setText(product.getSpecification());
        else
            infoArea.setText(product.getDescription());
    }

    @FXML
    private void handleButtonLeft(ActionEvent event){
        Image image = new Image("/images/asus.jpg");
        productShow.setImage(image);
    }

    @FXML
    private void handleButtonRight(ActionEvent event){
        Image image = new Image("/images/asus.jpg");
        productShow.setImage(image);
    }

    @FXML
    private void handleMakeOrderRequest(ActionEvent event){
        ObservableList<ProductInfoView> selectedItems = infoTable.getSelectionModel().getSelectedItems();
        if(!selectedItems.isEmpty()){
            stageManager.switchScene(FxmlView.MAKEORDER);
        }
    }

    @FXML
    private void handleGoBackRequest(ActionEvent event){
        stageManager.switchScene(FxmlView.MARKET);
    }

}
