package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AbstractDAO<T> {
    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public boolean insert(T t){
        Connection connection = null;
        Statement statement = null;
        boolean state = false;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                statement = connection.createStatement();
                statement.executeUpdate(this.createInsert(t));
                state = true;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return state;
    }

    private String createInsert(T t){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ").append(type.getSimpleName()).append(" (");
        for(Field field: type.getDeclaredFields()){
            if(field.getName().compareTo("ID") != 0)
                sb.append(field.getName()).append(", ");
        }
        sb.deleteCharAt(sb.lastIndexOf(", "));
        sb.append(") VALUES (");
        for(Field field: type.getDeclaredFields()){
            field.setAccessible(true);
            try {
                Object value = field.get(t);
                if(field.getName().compareTo("ID") != 0)
                    sb.append("'").append(value).append("',");
            }catch(IllegalArgumentException e){
                e.printStackTrace();
            }catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append(");");
        return sb.toString();
    }

    public boolean update(T t){
        Connection connection = null;
        Statement statement = null;
        boolean state = false;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                statement = connection.createStatement();
                statement.executeUpdate(this.createUpdate(t));
                state = true;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return state;
    }

    private String createUpdate(T t){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(type.getSimpleName()).append(" SET ");
        Integer id = 0;
        for(Field field: type.getDeclaredFields()){
            field.setAccessible(true);
            try {
                Object value = field.get(t);
                if(field.getName().compareTo("ID") != 0)
                    sb.append(field.getName()).append(" = '").append(value).append("', ");
                else
                    id = (Integer) value;
            }catch(IllegalArgumentException e){
                e.printStackTrace();
            }catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(", "));
        sb.append("WHERE ID = ").append(id.toString()).append(" ;");
        return sb.toString();
    }

    public boolean delete(T t){
        Connection connection = null;
        Statement statement = null;
        boolean state = false;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                statement = connection.createStatement();
                statement.executeUpdate(this.createDelete(t));
                state = true;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return state;
    }

    private String createDelete(T t){
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE ID = '");
        try {
            Field field = type.getDeclaredField("ID");
            field.setAccessible(true);
            sb.append(field.get(t).toString());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        sb.append("' ;");
        return sb.toString();
    }

    public T findByID(int ID){
        T searchfor = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM "+type.getSimpleName()+" WHERE ID = "+ID;
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);
                try {
                    List<T> findAll = new ArrayList<>();
                    while (resultSet.next()) {
                        T instance = type.newInstance();
                        for (Field field : type.getDeclaredFields()) {
                            Object value = resultSet.getObject(field.getName());
                            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                            Method method = propertyDescriptor.getWriteMethod();
                            method.invoke(instance, value);
                        }
                        findAll.add(instance);
                    }
                    if(!findAll.isEmpty())
                        searchfor = findAll.get(0);
                } catch(InstantiationException e) {
                    e.printStackTrace();
                } catch(IllegalAccessException e) {
                    e.printStackTrace();
                } catch(SecurityException e){
                    e.printStackTrace();
                } catch(IllegalArgumentException e){
                    e.printStackTrace();
                } catch(InvocationTargetException e) {
                    e.printStackTrace();
                } catch(SQLException e){
                    e.printStackTrace();
                } catch(IntrospectionException e) {
                    e.printStackTrace();
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return searchfor;
    }

    public List<T> findAllList(){
        List<T> findAll = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM "+type.getSimpleName();
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);
                try {
                    while (resultSet.next()) {
                        T instance = type.newInstance();
                        for (Field field : type.getDeclaredFields()) {
                            Object value = resultSet.getObject(field.getName());
                            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                            Method method = propertyDescriptor.getWriteMethod();
                            method.invoke(instance, value);
                        }
                        findAll.add(instance);
                    }
                } catch(InstantiationException e) {
                    e.printStackTrace();
                } catch(IllegalAccessException e) {
                    e.printStackTrace();
                } catch(SecurityException e){
                    e.printStackTrace();
                } catch(IllegalArgumentException e){
                    e.printStackTrace();
                } catch(InvocationTargetException e) {
                    e.printStackTrace();
                } catch(SQLException e){
                    e.printStackTrace();
                } catch(IntrospectionException e) {
                    e.printStackTrace();
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return findAll;
    }

    public ObservableList<T> findAllObservableList(){
        ObservableList<T> findAll = FXCollections.observableArrayList();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM "+type.getSimpleName();
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);
                try {
                    while (resultSet.next()) {
                        T instance = type.newInstance();
                        for (Field field : type.getDeclaredFields()) {
                            Object value = resultSet.getObject(field.getName());
                            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                            Method method = propertyDescriptor.getWriteMethod();
                            method.invoke(instance, value);
                        }
                        findAll.add(instance);
                    }
                } catch(InstantiationException e) {
                    e.printStackTrace();
                } catch(IllegalAccessException e) {
                    e.printStackTrace();
                } catch(SecurityException e){
                    e.printStackTrace();
                } catch(IllegalArgumentException e){
                    e.printStackTrace();
                } catch(InvocationTargetException e) {
                    e.printStackTrace();
                } catch(SQLException e){
                    e.printStackTrace();
                } catch(IntrospectionException e) {
                    e.printStackTrace();
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return findAll;
    }

    public TableView<T> createTable(){
        TableView<T> tableView = new TableView<>();
        for (Field field : type.getDeclaredFields()) {
            TableColumn<T, ?> newCol = new TableColumn<>(field.getName());
            newCol.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
            tableView.getColumns().add(newCol);
        }
        ObservableList<T> tableContent = findAllObservableList();
        tableView.setItems(tableContent);
        return tableView;
    }
}
