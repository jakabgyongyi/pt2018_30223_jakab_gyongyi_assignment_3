package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import com.tp.hwk.model.Address;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class AddressDAO extends AbstractDAO<Address> {

    public Address findByClientID(int clientID){
        Address address = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM address WHERE clientID = "+clientID+";";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                if(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    clientID = resultSet.getInt("clientID");
                    String city = resultSet.getString("city");
                    String street = resultSet.getString("street");
                    int number = resultSet.getInt("number");
                    int postcode = resultSet.getInt("postcode");
                    address = new Address(ID, clientID, city, street, number, postcode);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return address;
    }

}
