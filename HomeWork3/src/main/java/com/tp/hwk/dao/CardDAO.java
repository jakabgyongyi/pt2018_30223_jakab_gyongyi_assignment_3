package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import com.tp.hwk.model.Card;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class CardDAO extends AbstractDAO<Card> {

    public Card findByClientID(int clientID){
        Card card = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM card WHERE clientID = "+clientID+";";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                if(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    clientID = resultSet.getInt("clientID");
                    String brand = resultSet.getString("brand");
                    String accountNumber = resultSet.getString("accountNumber");
                    int cardSecurityNumber = resultSet.getInt("cardSecurityNumber");
                    String cardHolderName = resultSet.getString("cardHolderName");
                    card = new Card(ID,clientID,brand,accountNumber,cardSecurityNumber,cardHolderName);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return card;
    }

}
