package com.tp.hwk.dao;

import com.tp.hwk.model.Category;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDAO extends AbstractDAO<Category> {
}
