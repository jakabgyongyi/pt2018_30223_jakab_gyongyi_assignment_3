package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import com.tp.hwk.model.Customer;
import com.tp.hwk.model.specialModels.CustomerOrderView;
import javafx.collections.ObservableList;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class CustomerDAO extends AbstractDAO<Customer> {

    public Customer customerLogin(String userName, String password){
        Customer customer =null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null) {
                System.out.println("Connection established!");
                String sql = "SELECT * FROM customer WHERE userName = '"+userName+"' AND password = '"+password+"';";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);
                if(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    String lastName = resultSet.getString("lastName");
                    String firstName = resultSet.getString("firstName");
                    userName = resultSet.getString("userName");
                    password = resultSet.getString("password");
                    customer = new Customer(ID, lastName, firstName, userName,password);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return customer;
    }

    public Customer findByUserName(String userName){
        Customer customer =null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null) {
                System.out.println("Connection established!");
                String sql = "SELECT * FROM customer WHERE userName = '"+userName+"';";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);
                if(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    String lastName = resultSet.getString("lastName");
                    String firstName = resultSet.getString("firstName");
                    userName = resultSet.getString("userName");
                    String password = resultSet.getString("password");
                    customer = new Customer(ID, lastName, firstName, userName,password);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return customer;
    }

    public ObservableList<CustomerOrderView> findAllOrders(int ID){
        return null;
    }
}
