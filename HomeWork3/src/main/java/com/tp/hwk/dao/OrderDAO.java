package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import com.tp.hwk.model.ProOrder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderDAO extends AbstractDAO<ProOrder> {

    public List<ProOrder> findByClientID(int clientID){
        List<ProOrder> orders = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM proorder WHERE clientID = "+clientID+";";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                while(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    clientID = resultSet.getInt("clientID");
                    int productID = resultSet.getInt("productID");
                    Timestamp dateTime = resultSet.getTimestamp("dateTime");
                    int quantity = resultSet.getInt("quantity");
                    double totalPrice = resultSet.getDouble("totalPrice");
                    String shipping = resultSet.getString("shipping");
                    ProOrder order = new ProOrder(ID, clientID, productID, dateTime, quantity, totalPrice, shipping);
                    orders.add(order);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return orders;
    }

    public List<ProOrder> findByProductID(int productID){
        List<ProOrder> orders = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM proorder WHERE productID = "+productID+";";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                while(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    int clientID = resultSet.getInt("clientID");
                    productID = resultSet.getInt("productID");
                    Timestamp dateTime = resultSet.getTimestamp("dateTime");
                    int quantity = resultSet.getInt("quantity");
                    double totalPrice = resultSet.getDouble("totalPrice");
                    String shipping = resultSet.getString("shipping");
                    ProOrder order = new ProOrder(ID, clientID, productID, dateTime, quantity, totalPrice, shipping);
                    orders.add(order);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return orders;
    }
}
