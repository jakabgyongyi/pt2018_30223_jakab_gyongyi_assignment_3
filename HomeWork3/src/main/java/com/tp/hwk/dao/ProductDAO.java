package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import com.tp.hwk.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class ProductDAO extends AbstractDAO<Product> {

    public Product findByName(String name){
        Product searchfor = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM product WHERE name = '"+name+"';";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                if(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    int subCategoryID = resultSet.getInt("subCategoryID");
                    name = resultSet.getString("name");
                    String specification = resultSet.getString("specification");
                    String description = resultSet.getString("description");
                    searchfor = new Product(ID, subCategoryID, name, specification, description);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return searchfor;
    }

    public ObservableList<Product> findAllBySubCatofyId(int subCategoryID){
        ObservableList<Product> searchfor = FXCollections.observableArrayList();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM product WHERE  subCategoryID= "+subCategoryID+";";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                while(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    subCategoryID = resultSet.getInt("subCategoryID");
                    String name = resultSet.getString("name");
                    String specification = resultSet.getString("specification");
                    String description = resultSet.getString("description");
                    Product newproduct = new Product(ID, subCategoryID, name, specification, description);
                    searchfor.add(newproduct);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return searchfor;
    }
}
