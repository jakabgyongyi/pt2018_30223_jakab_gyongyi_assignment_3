package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import com.tp.hwk.model.Provider;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class ProviderDAO extends AbstractDAO<Provider> {

    public Provider findByName(String name){
        Provider searchfor = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM provider WHERE providerName = '"+name+"';";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                if(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    String providerName = resultSet.getString("providerName");
                    String address = resultSet.getString("address");
                    String contact = resultSet.getString("contact");
                    String bankaccount = resultSet.getString("bankaccount");
                    searchfor = new Provider(ID, providerName, address, contact, bankaccount);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return searchfor;
    }
}
