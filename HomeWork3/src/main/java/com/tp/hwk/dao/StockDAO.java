package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import com.tp.hwk.model.Stock;
import com.tp.hwk.model.specialModels.ProductInfoView;
import javafx.collections.ObservableList;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StockDAO extends AbstractDAO<Stock> {

    public List<Stock> findByProductID(int productID){
        List<Stock> stocks = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM stock WHERE productID = "+productID+";";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                while(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    productID = resultSet.getInt("productID");
                    int providerID = resultSet.getInt("providerID");
                    double price = resultSet.getDouble("price");
                    double discount = resultSet.getDouble("discount");
                    int guarantee = resultSet.getInt("guarantee");
                    int quantity = resultSet.getInt("quantity");
                    Stock newStock = new Stock(ID, productID,providerID,price,discount,guarantee,quantity);
                    stocks.add(newStock);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return stocks;
    }

    public List<Stock> findByProviderID(int providerID){
        List<Stock> stocks = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM stock WHERE providerID = "+providerID+";";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                while(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    int productID = resultSet.getInt("productID");
                    providerID = resultSet.getInt("providerID");
                    double price = resultSet.getDouble("price");
                    double discount = resultSet.getDouble("discount");
                    int guarantee = resultSet.getInt("guarantee");
                    int quantity = resultSet.getInt("quantity");
                    Stock newStock = new Stock(ID, productID,providerID,price,discount,guarantee,quantity);
                    stocks.add(newStock);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return stocks;
    }

    public Stock findByProviderIDAndProductID(int providerID, int productID){
        Stock stock = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM stock WHERE providerID = "+providerID+" AND productID = "+productID+" ;";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                if(resultSet.next()) {
                        int ID = resultSet.getInt("ID");
                    productID = resultSet.getInt("productID");
                    providerID = resultSet.getInt("providerID");
                    double price = resultSet.getDouble("price");
                    double discount = resultSet.getDouble("discount");
                    int guarantee = resultSet.getInt("guarantee");
                    int quantity = resultSet.getInt("quantity");
                    stock = new Stock(ID, productID,providerID,price,discount,guarantee,quantity);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return stock;
    }


}
