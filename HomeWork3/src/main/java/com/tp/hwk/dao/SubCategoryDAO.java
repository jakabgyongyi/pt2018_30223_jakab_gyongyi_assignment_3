package com.tp.hwk.dao;

import com.tp.hwk.connection.ConnectionFactory;
import com.tp.hwk.model.SubCategory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class SubCategoryDAO extends AbstractDAO<SubCategory> {

    public SubCategory findByName(String name){
        SubCategory searchfor = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM subcategory WHERE name = '"+name+"';";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                if(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    int categoryID = resultSet.getInt("categoryID");
                    name = resultSet.getString("name");
                    searchfor = new SubCategory(ID, categoryID, name);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return searchfor;
    }

    public ObservableList<SubCategory> findByCategoryID(int categoryID){
        ObservableList<SubCategory> subcats = FXCollections.observableArrayList();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            if(connection != null){
                System.out.println("Connection established!");
                String sql = "SELECT * FROM subcategory WHERE categoryID = "+ categoryID +";";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                while(resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    categoryID = resultSet.getInt("categoryID");
                    String name = resultSet.getString("name");
                    SubCategory newSubCat = new SubCategory(ID, categoryID, name);
                    subcats.add(newSubCat);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
            ConnectionFactory.close(resultSet);
        }
        return subcats;
    }
}
