package com.tp.hwk.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    private int clientID;
    private String city;
    private String street;
    private int number;
    private int postcode;

    public Address(){
        this.ID = 0;
        this.clientID = 0;
        this.city = null;
        this.street = null;
        this.number = 0;
        this.postcode = 0;
    }

    public Address(int ID, int clientID, String city, String street, int number, int postcode) {
        this.ID = ID;
        this.clientID = clientID;
        this.city = city;
        this.street = street;
        this.number = number;
        this.postcode = postcode;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ").append(this.ID).append("\n");
        sb.append("ClientID: ").append(this.clientID).append("\n");
        sb.append("City: ").append(this.city).append("\n");
        sb.append("Street: ").append(this.street).append("\n");
        sb.append("Number: ").append(this.number).append("\n");
        sb.append("Postcode: ").append(this.postcode).append("\n");
        return sb.toString();
    }
}
