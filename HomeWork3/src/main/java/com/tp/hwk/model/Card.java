package com.tp.hwk.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    private int clientID;
    private String brand;
    private String accountNumber;
    private int cardSecurityNumber;
    private String cardHolderName;

    public Card(){
        this.ID = 0;
        this.clientID = 0;
        this.brand = null;
        this.accountNumber = null;
        this.cardSecurityNumber = 0;
        this.cardHolderName = null;
    }

    public Card(int ID, int clientID, String brand, String accountNumber, int cardSecurityNumber, String cardHolderName) {
        this.ID = ID;
        this.clientID = clientID;
        this.brand = brand;
        this.accountNumber = accountNumber;
        this.cardSecurityNumber = cardSecurityNumber;
        this.cardHolderName = cardHolderName;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClient_ID(int clientID) {
        this.clientID = clientID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getCardSecurityNumber() {
        return cardSecurityNumber;
    }

    public void setCardSecurityNumber(int cardSecurityNumber) {
        this.cardSecurityNumber = cardSecurityNumber;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }
}
