package com.tp.hwk.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    private int clientID;
    private String email;
    private String telnumber;

    public Contact(){
        this.ID = 0;
        this.clientID = 0;
        this.email = null;
        this.telnumber = null;
    }

    public Contact(int ID, int clientID, String email, String telnumber) {
        this.ID = ID;
        this.clientID = clientID;
        this.email = email;
        this.telnumber = telnumber;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelnumber() {
        return telnumber;
    }

    public void setTelnumber(String telnumber) {
        this.telnumber = telnumber;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Contact ID: ").append(this.getID()).append("\n");
        sb.append("Client ID: ").append(this.getClientID()).append("\n");
        sb.append("Email: ").append(this.getEmail()).append("\n");
        sb.append("Phone Number").append(this.getTelnumber()).append("\n");
        return sb.toString();
    }
}
