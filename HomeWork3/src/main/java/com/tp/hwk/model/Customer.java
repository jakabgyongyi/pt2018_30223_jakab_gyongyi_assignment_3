package com.tp.hwk.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    private String lastName;
    private String firstName;
    private String userName;
    private String password;

    public Customer(){
        this.ID = 0;
        this.lastName = null;
        this.firstName = null;
        this.userName = null;
        this.password = null;
    }

    public Customer(int ID, String lastName, String firstName, String userName, String password) {
        this.ID = ID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.userName = userName;
        this.password = password;
    }


    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Customer ID: ").append(this.getID()).append("\n");
        sb.append("Last name: ").append(this.getLastName()).append("\n");
        sb.append("First name: ").append(this.getFirstName()).append("\n");
        sb.append("User name: ").append(this.getUserName()).append("\n");
        sb.append("Password: ").append(this.getPassword()).append("\n");
        return sb.toString();
    }
}
