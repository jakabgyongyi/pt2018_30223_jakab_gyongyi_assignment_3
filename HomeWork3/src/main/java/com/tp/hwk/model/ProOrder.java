package com.tp.hwk.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class ProOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    private int clientID;
    private int stockID;
    private Timestamp dateTime;
    private int quantity;
    private double totalPrice;
    private String shipping;

    public ProOrder(){
        this.ID = 0;
        this.clientID = 0;
        this.stockID = 0;
        this.dateTime = new Timestamp(System.currentTimeMillis());
        this.quantity = 0;
        this.totalPrice = 0.0;
        this.shipping = null;
    }

    public ProOrder(int ID, int clientID, int stockID, Timestamp dateTime, int quantity, double totalPrice, String shipping) {
        this.ID = ID;
        this.clientID = clientID;
        this.stockID = stockID;
        this.dateTime = dateTime;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.shipping = shipping;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public int getStockID() {
        return stockID;
    }

    public void setStockID(int stockID) {
        this.stockID = stockID;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }
}
