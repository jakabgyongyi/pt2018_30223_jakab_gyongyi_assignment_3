package com.tp.hwk.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
   private int ID;
   private int subCategoryID;
   private String name;
   private String specification;
   private String description;

    public Product(){
        this.ID = 0;
        this.subCategoryID = 0;
        this.name = null;
        this.specification = null;
        this.description = null;
    }

    public Product(int ID, int subCategoryID, String name, String specification, String description) {
        this.ID = ID;
        this.subCategoryID = subCategoryID;
        this.name = name;
        this.specification = specification;
        this.description = description;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getSubCategoryID() {
        return subCategoryID;
    }

    public void setSubCategoryID(int subCategoryID) {
        this.subCategoryID = subCategoryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


