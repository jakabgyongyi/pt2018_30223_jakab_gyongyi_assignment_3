package com.tp.hwk.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Provider {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    private String providerName;
    private String address;
    private String contact;
    private String bankaccount;

    public Provider(){
        this.ID = 0;
        this.providerName = null;
        this.address = null;
        this.contact = null;
        this.bankaccount = null;
    }

    public Provider(int ID, String providerName, String address, String contact, String bankaccount) {
        this.ID = ID;
        this.providerName = providerName;
        this.address = address;
        this.contact = contact;
        this.bankaccount = bankaccount;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(String bankaccount) {
        this.bankaccount = bankaccount;
    }
}
