package com.tp.hwk.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    private int productID;
    private int providerID;
    private double price;
    private double discount;
    private int guarantee;
    private int quantity;

    public Stock(){
        this.ID = 0;
        this.productID = 0;
        this.providerID = 0;
        this.price = 0.0;
        this.discount = 0.0;
        this.guarantee = 0;
        this.quantity = 0;
    }

    public Stock(int ID, int productID, int providerID, double price, double discount, int guarantee, int quantity) {
        this.ID = ID;
        this.productID = productID;
        this.providerID = providerID;
        this.price = price;
        this.discount = discount;
        this.guarantee = guarantee;
        this.quantity = quantity;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getProviderID() {
        return providerID;
    }

    public void setProviderID(int providerID) {
        this.providerID = providerID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getGuarantee() {
        return guarantee;
    }

    public void setGuarantee(int guarantee) {
        this.guarantee = guarantee;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
