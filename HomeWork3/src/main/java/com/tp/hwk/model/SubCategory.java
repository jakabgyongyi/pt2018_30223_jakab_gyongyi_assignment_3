package com.tp.hwk.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SubCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    private int categoryID;
    private String name;

    public SubCategory(){
        this.ID = 0;
        this.categoryID = 0;
        this.name = null;
    }

    public SubCategory(int ID, int categoryID, String name) {
        this.ID = ID;
        this.categoryID = categoryID;
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
