package com.tp.hwk.model.specialModels;

import java.sql.Timestamp;

public class CustomerOrderView {
    private String productName;
    private String providerName;
    private Timestamp time;
    private double price;
    private double discount;
    private double finalPrice;
    private int guarantee;

    public CustomerOrderView() {
        super();
    }

    public CustomerOrderView(String productName, String providerName, Timestamp time, double price, double discount, double finalPrice, int guarantee) {
        this.productName = productName;
        this.providerName = providerName;
        this.time = time;
        this.price = price;
        this.discount = discount;
        this.finalPrice = finalPrice;
        this.guarantee = guarantee;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getGuarantee() {
        return guarantee;
    }

    public void setGuarantee(int guarantee) {
        this.guarantee = guarantee;
    }
}
