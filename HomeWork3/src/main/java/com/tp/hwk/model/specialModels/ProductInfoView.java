package com.tp.hwk.model.specialModels;

public class ProductInfoView {
    private String providerName;
    private double price;
    private double discount;
    private double finalPrice;
    private int guarantee;
    private String stock;

    public ProductInfoView() {
        super();
    }

    public ProductInfoView(String providerName, double price, double discount, double finalPrice, int guarantee, String stock) {
        this.providerName = providerName;
        this.price = price;
        this.discount = discount;
        this.finalPrice = finalPrice;
        this.guarantee = guarantee;
        this.stock = stock;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getGuarantee() {
        return guarantee;
    }

    public void setGuarantee(int guarantee) {
        this.guarantee = guarantee;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}
