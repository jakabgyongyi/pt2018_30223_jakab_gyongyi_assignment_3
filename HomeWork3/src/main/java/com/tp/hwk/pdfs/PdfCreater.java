package com.tp.hwk.pdfs;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tp.hwk.model.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class PdfCreater {
    private static String PATH = "C:\\Users\\jakabgyongyi\\IdeaProjects\\HomeWork3\\src\\main\\java\\com\\tp\\hwk\\pdfs\\";
    private static Font logoFont = new Font(Font.FontFamily.TIMES_ROMAN, 30, Font.BOLDITALIC, BaseColor.ORANGE);
    private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 24, Font.ITALIC, BaseColor.GRAY);
    private static Font headingOneFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font headingTwoFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.UNDERLINE);
    private static Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
    private static Font coloredFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.GREEN);

    public static void createPDF(String NAME, ArrayList<Object> elements){
        Product product = (Product) elements.get(0);
        Provider provider = (Provider) elements.get(1);
        Customer customer = (Customer) elements.get(2);
        Address address = (Address) elements.get(3);
        Contact contact = (Contact) elements.get(4);
        ProOrder proOrder = (ProOrder) elements.get(5);
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(PATH+NAME));
            document.open();
            addMetaData(document);
            document.add(new Paragraph("E-SHOP", logoFont));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Order Bill", titleFont));
            document.add(new Paragraph("\n"));
            addProvider(document, provider);
            addCustomer(document, customer);
            addAddress(document, address);
            addContact(document, contact);
            addTable(document, product, proOrder);
            addBottom(document, proOrder);
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void addMetaData(Document document){
        document.addTitle("Order bill");
        document.addSubject("Creating PDF");
        document.addKeywords("Provider, Customer, Product");
        document.addAuthor("Jakab Gyongyi Aniko");
        document.addCreator("Jakab Gyongyi Aniko");
    }

    private static void addProvider(Document document, Provider provider) throws DocumentException {
        document.add(new Paragraph("PROVIDER", headingOneFont));
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(provider.getProviderName()).append("\n");
        sb.append("Nr.ord.reg. com./an: J40/372/2002").append("\n");
        sb.append("C.I.F: ").append("RO 14399840").append("\n");
        sb.append("BankAccount: ").append(provider.getBankaccount()).append("\n");
        sb.append("Address: ").append(provider.getAddress()).append("\n");
        sb.append("Contact: ").append(provider.getContact()).append("\n");
        document.add(new Paragraph(sb.toString(), normalFont));
    }

    private static void addCustomer(Document document, Customer customer)throws DocumentException{
        document.add(new Paragraph("CUSTOMER", headingOneFont));
        document.add(new Paragraph("Personal Information", headingTwoFont));
        StringBuilder sb = new StringBuilder();
        sb.append("Last Name: ").append(customer.getLastName()).append("\n");
        sb.append("First Name: ").append(customer.getFirstName()).append("\n");
        document.add(new Paragraph(sb.toString(), normalFont));
    }

    private static void addAddress(Document document, Address address) throws  DocumentException{
        document.add(new Paragraph("Address Information", headingTwoFont));
        StringBuilder sb = new StringBuilder();
        sb.append("City: ").append(address.getCity()).append("\n");
        sb.append("Street: ").append(address.getStreet()).append("\n");
        sb.append("Number: ").append(address.getNumber()).append("\n");
        sb.append("Postcode: ").append(address.getPostcode()).append("\n");
        document.add(new Paragraph(sb.toString(), normalFont));
    }

    private static void addContact(Document document, Contact contact) throws DocumentException{
        document.add(new Paragraph("Contact Information", headingTwoFont));
        StringBuilder sb = new StringBuilder();
        sb.append("E-Mail: ").append(contact.getEmail()).append("\n");
        sb.append("Phone Number: ").append(contact.getTelnumber()).append("\n");
        document.add(new Paragraph(sb.toString(), normalFont));
    }

    private static void addTable(Document document, Product product, ProOrder proOrder) throws DocumentException{
        document.add(new Paragraph("PRODUCT & ORDER", headingOneFont));
        PdfPTable table = new PdfPTable(3);
        PdfPCell cell = new PdfPCell(new Phrase("Product Name"));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Quantity"));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total Price"));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        table.addCell(product.getName());
        table.addCell(String.valueOf(proOrder.getQuantity()));
        table.addCell(String.valueOf(proOrder.getTotalPrice()));

        document.add(table);
        document.add(new Paragraph("\n", headingOneFont));
    }

    private static void addBottom(Document document, ProOrder proOrder) throws DocumentException{
        document.add(new Paragraph("This bill is take into consideration with or without the signiture of the customer that made the payment", coloredFont));
        document.add(new Paragraph("Date and exact time of order: "+proOrder.getDateTime(), normalFont));
        document.add(new Paragraph("Shipping method: "+proOrder.getShipping(), normalFont));
        document.add(new Paragraph("\n", logoFont));
        document.add(new Paragraph("SIGNATURE: ", headingTwoFont));
    }
}
