package com.tp.hwk.view;

import java.util.ResourceBundle;

public enum FxmlView {

        CLIENTLOGIN {
            @Override
            public String getTitle() {
                return getStringFromResourceBundle("clientlogin.title");
            }

            @Override
            public String getFxmlFile() {
                return "/fxml/ClientLogin.fxml";
            }
        }, CLIENTACCOUNT {
            @Override
            public String getTitle() {
                return getStringFromResourceBundle("clientaccount.title");
            }

            @Override
            public String getFxmlFile() {
                return "/fxml/ClientAccount.fxml";
            }
        }, CLIENTSIGNUP{
            @Override
            public String getTitle() { return getStringFromResourceBundle("clientsignup.title"); }

            @Override
            public String getFxmlFile() { return "/fxml/ClientSignup.fxml"; }
        }, ADMINACCOUNT{
            @Override
            public String getTitle() { return getStringFromResourceBundle("adminaccount.title"); }

            @Override
            public String getFxmlFile() { return "/fxml/AdminAccount.fxml"; }
        }, MAKEORDER{
            @Override
            public String getTitle() { return getStringFromResourceBundle("makeorder.title"); }

            @Override
            public String getFxmlFile() { return "/fxml/MakeOrder.fxml"; }
        }, MARKET{
            @Override
            public String getTitle() { return getStringFromResourceBundle("market.title"); }

            @Override
            public String getFxmlFile() { return "/fxml/Market.fxml"; }
        }, PRODUCTVIEW{
            @Override
            public String getTitle() { return getStringFromResourceBundle("productview.title"); }

            @Override
            public String getFxmlFile() { return "/fxml/ProductView.fxml"; }
        };

        public abstract String getTitle();
        public abstract String getFxmlFile();

        String getStringFromResourceBundle(String key){
            return ResourceBundle.getBundle("Bundle").getString(key);
        }
}
